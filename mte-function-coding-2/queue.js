let collection = [];

// Write the queue functions below.

function print() {
    return collection;
}

function enqueue(element) {
    collection[collection.length] = element;
    return collection;
}

function dequeue() {
    if (collection.length === 0) {
        return "Queue is empty";
    }
    const dequeuedElement = collection.shift();
    return dequeuedElement;
}

function front() {
    if (collection.length === 0) {
        return "Queue is empty";
    }
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};